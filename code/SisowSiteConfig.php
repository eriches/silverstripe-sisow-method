<?php

class SisowSiteConfig extends DataExtension {

    static $db = array(
    );

    private static $has_many = array(
        'SisowMethods' => 'SisowMethod'
    );

    public function updateCMSFields(FieldList $fields)
    {

        $SisowMethodsGridField = new GridField(
            'SisowMethods',
            _t("SiteConfig.SISOWMETHODS", "Sisow Methods"),
            $this->owner->SisowMethods(),
            GridFieldConfig::create()
                ->addComponent(new GridFieldToolbarHeader())
                ->addComponent(new GridFieldAddNewButton("toolbar-header-right"))
                ->addComponent(new GridFieldSortableHeader())
                ->addComponent(new GridFieldDataColumns())
                ->addComponent(new GridFieldPaginator(50))
                ->addComponent(new GridFieldEditButton())
                ->addComponent(new GridFieldDeleteAction())
                ->addComponent(new GridFieldDetailForm())
                ->addComponent(new GridFieldFilterHeader())
                ->addComponent(new GridFieldOrderableRows('SortOrder'))
        );

        if (Permission::check('ADMIN')) {
            $fields->addFieldsToTab("Root." . _t("SiteConfig.SISOWMETHODS", "Sisow Methods"), array(
                $SisowMethodsGridField
            ));

        }

    }

}